package com.tcs.poc.addresses.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.poc.addresses.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> { 
	List<Address> findByPersonId(int personId);
}
