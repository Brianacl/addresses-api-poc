package com.tcs.poc.addresses.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.tcs.poc.addresses.repository.AddressRepository;

import reactor.core.publisher.Mono;

import com.tcs.poc.addresses.exception.ResourceNotFoundException;
import com.tcs.poc.addresses.model.Address;

@Service
public class AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private WebClient.Builder webClientBuilder;
	
	@Async
	public CompletableFuture<List<Address>> getAllAddresses() {
		return CompletableFuture.completedFuture(
				this.addressRepository.findAll()
				);
	}
	
	@Async 
	public CompletableFuture<List<Address>> getAddressByPerson(final int personId) {
		return CompletableFuture.completedFuture(
				this.addressRepository.findByPersonId(personId)
				);
	}
	
	@Async
	public CompletableFuture<Address> getAddresById(final int addressId) 
		throws ResourceNotFoundException {
		
		return CompletableFuture.completedFuture(
				this.addressRepository
				.findById(addressId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Address not found on :: " + addressId)
						)
				);
	}
	
	@Async
	public CompletableFuture<Address> createAddress(final Address address) 
		throws ResourceNotFoundException {
		webClientBuilder.build()
			.get()
			.uri("http://gateway-service/people/api/v1/person/" + address.getPersonId())
			.retrieve()
			.onStatus(HttpStatus::is4xxClientError, response -> Mono.error(new ResourceNotFoundException("Person not found.")))
			.bodyToMono(Address.class)
			.block();
		
		return CompletableFuture.completedFuture(
				this.addressRepository.save(address)
				);
	}
	
	@Async
	public CompletableFuture<Address> updateAddress(final int addressId, final Address addressDetails)
		throws ResourceNotFoundException {
		Address address = this.addressRepository
				.findById(addressId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Address not found on :: " + addressId)
						);

		address.setAddress(addressDetails.getAddress());
		address.setDistrict(addressDetails.getDistrict());
		address.setCity(addressDetails.getCity());
		address.setPostalCode(addressDetails.getPostalCode());
		
		return CompletableFuture.completedFuture(
				this.addressRepository.save(address)
				);
	}
	
	@Async
	public CompletableFuture<Map<String, Boolean>> deleteAddress(final int addressId)
		throws ResourceNotFoundException {
		Address address = this.addressRepository
				.findById(addressId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Address not found on:: " + addressId)
						);

		this.addressRepository.delete(address);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return CompletableFuture.completedFuture(response);
	}
}
