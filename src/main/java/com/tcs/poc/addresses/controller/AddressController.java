package com.tcs.poc.addresses.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.tcs.poc.addresses.exception.ResourceNotFoundException;
import com.tcs.poc.addresses.model.Address;
import com.tcs.poc.addresses.service.AddressService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Address", description = "Address API")
public class AddressController {
	
	@Autowired
	private AddressService addressService;
	
	@Operation(summary = "Find all addresses")
	@GetMapping(value= "/addresses", produces = { "application/json"})
	public CompletableFuture<ResponseEntity<List<Address>>> getAllAddresses() {
		return this.addressService.getAllAddresses()
				.<ResponseEntity<List<Address>>>thenApply(ResponseEntity::ok)
				.exceptionally(getAllAddressFailure);
	}
	
	private static Function<Throwable,? 
			extends ResponseEntity<List<Address>>> getAllAddressFailure = throwable ->
				ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	

	@Operation(summary = "Find an address by personId")
	@GetMapping(value = "/address", produces = { "application/json" })
	public CompletableFuture<ResponseEntity<List<Address>>> getAddressByPerson(
			@RequestParam(value = "personId", defaultValue = "0") int personId
			) {
		
		return this.addressService.getAddressByPerson(personId)
				.<ResponseEntity<List<Address>>>thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Find an address by id")
	@GetMapping(value = "/address/{id}", produces = { "application/json" })
	public CompletableFuture<ResponseEntity<Address>> getAddressById(
			@Parameter(description="The address id must be provided to find a record (required).", required = true)
			@PathVariable(value = "id") Integer addressId
			)
		throws ResourceNotFoundException {
		
		return this.addressService.getAddresById(addressId)
				.<ResponseEntity<Address>>thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Add a new address", tags = {"address"})
	@PostMapping(value = "/address", consumes = { "application/json" })
	public CompletableFuture<ResponseEntity<Address>> createAddress(
			@Parameter(description = "Address to add",
			required = true, schema = @Schema(implementation = Address.class,
			example = 
					"{ \"address\":\"string\", \"district\": \"string\", \"city\":\"string\", \"postalCode\": \"string\", \"personId\": 0 }"))
			@Valid @RequestBody Address address
			) throws InterruptedException, ExecutionException, ResourceNotFoundException {
		
		return this.addressService.createAddress(address)
				.<ResponseEntity<Address>> thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Update an existing address", tags = {"address"})
	@PutMapping(value = "/address/{id}", consumes = { "application/json"})
	public CompletableFuture<ResponseEntity<Address>> updateAddress(
			@Parameter(description="Address id required to be updated. Cannot be empty.", 
        		required=true)
			@PathVariable(value = "id") Integer addressId,
			@Parameter(description = "Address to add",
			required = true, schema = @Schema(implementation = Address.class,
			example = 
			"{ \"address\":\"string\", \"district\": \"string\", \"city\":\"string\", \"postalCode\": \"string\"}"))
			@Valid @RequestBody Address addressDetails
			)
		throws ResourceNotFoundException {
		
		return this.addressService.updateAddress(addressId, addressDetails)
				.<ResponseEntity<Address>>thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Deletes an address", tags = { "address" })
	@DeleteMapping("/address/{id}")
	public CompletableFuture<ResponseEntity<Map<String, Boolean>>> deleteAddress(
			@Parameter(description="Address id required to be deleted. Cannot be empty.",
        		required=true)
			@PathVariable(value = "id") Integer addressId
			)
			throws Exception {
		
		
		return this.addressService.deleteAddress(addressId)
				.<ResponseEntity<Map<String, Boolean>>>thenApply(ResponseEntity::ok);
	}
}
